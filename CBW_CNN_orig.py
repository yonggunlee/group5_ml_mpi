from __future__ import print_function
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split
from mpi4py import MPI
import h5py
import numpy as np

batch_size = 128
num_classes = 2
epochs = 2

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

data_file_name = 'CBW_data_down.mat'
DATA_FOLDER_PATH = '/home/yonggunlee/group'

FILE_PATH = DATA_FOLDER_PATH + '/' + data_file_name

# mat_2 = scipy.io.loadmat(FILE_PATH)
mat_2 = h5py.File(FILE_PATH)
mat_2.keys()

data_x = mat_2['data']
data_y = mat_2['label']
data_y = to_categorical(data_y, 2)

data = np.array(data_x)
label = np.array(data_y)
data = np.transpose(data, (0, 2, 1))
data = np.expand_dims(data, axis=3)
# input image dimensions
img_rows, img_cols = 32, 128

input_shape = (img_rows, img_cols, 1)
x_train, x_test, y_train, y_test = train_test_split(data, label, test_size=0.20, random_state=42)
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')

print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
# y_train = keras.utils.to_categorical(y_train, num_classes)
# y_test = keras.utils.to_categorical(y_test, num_classes)
t_start = MPI.Wtime()

model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=1,
              validation_data=(x_test, y_test))
Model1score = model.evaluate(x_test, y_test, verbose=0)
    #print('Test loss:', Model1score[0])
    #print('Test accuracy:', Model1score[1])

model = Sequential()
model.add(Conv2D(32, kernel_size=(5, 5),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
                optimizer=keras.optimizers.Adadelta(),
                metrics=['accuracy'])

model.fit(x_train, y_train,
           batch_size=batch_size,
           epochs=epochs,
           verbose=1,
           validation_data=(x_test, y_test))
Model2score = model.evaluate(x_test, y_test, verbose=0)
    #print('Test loss:', Model2score[0])
    #print('Test accuracy:', Model2score[1])

t_end = MPI.Wtime()
print('Model 1 Accuracy: ', Model1score[1])
print('Model 2 Accuracy: ', Model2score[1])
print('Total execution time with MPI', t_end-t_start)

