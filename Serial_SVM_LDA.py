# Machine learning program using LDA and SVM algorithms

import pandas as pd
import numpy as np
import sys
import math
from mpi4py import MPI
from mpi4py.MPI import ANY_SOURCE
import matplotlib.pyplot as plt
import seaborn as sns
import h5py


## MPI communicator init
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# Import ML libraries
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, roc_auc_score
from sklearn.feature_selection import VarianceThreshold
from sklearn.preprocessing import StandardScaler

from sklearn.linear_model import LinearRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn import svm
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error, roc_curve


start = MPI.Wtime()

data_file_name = 'CBW_data_down_reshape.mat'
mat = h5py.File(data_file_name)
mat.keys()

data = mat['data']
label = mat['label']

data= np.array(data)
label= np.array(label)

X = data
y = label


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# SVM
svm = svm.SVC(gamma=0.001)
svm.fit(X_train, y_train)
y_predict = svm.predict(X_test)

Acu1 = accuracy_score(y_test, y_predict)

print("\nSVM alg:")
print("Accuracy ", Acu1)


# Linear Discriminant Analysis (LDA)
lda = LDA(n_components=1)

lda.fit(X_train, y_train) #.astype('int'))
y_predict_lda = lda.predict(X_test)

Acu2 = accuracy_score(y_test, y_predict_lda)

print("\nLDA alg:")
print("Accuracy: ", Acu2)


print("Time taken to Complete: ", MPI.Wtime() - start)